(in-package :cl-user)

(defpackage webapp-test
  (:use :cl
        :webapp
        :prove))
(in-package :webapp-test)

(plan 3)

(ok (not (find 4 '(1 2 3))))

(is 4 4)

(isnt 1 #\1)

(finalize)
