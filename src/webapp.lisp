;;;; webapp.lisp

(in-package :cl-user)

(defpackage webapp
  (:use :cl)
  (:import-from :alexandria
                :starts-with-subseq)
  (:import-from :lack.builder
                :builder)
  (:export :start
           :stop))
(in-package :webapp)

(defun app (env)
  (declare (ignore env))
  `(200
    ;; Header
    (:content-type "text/plain")
    ;; Body
    ("Hello Clack !!!")))

(defvar *root-directory*
  (asdf:system-relative-pathname "webapp" ""))

(defun start ()
  (defparameter *clack-server*
    (clack:clackup
     (builder
      :session
      (:static
       :path "/public/"
       :root #p"/static/")
      #'app))))

(defun stop ()
  (clack:stop *clack-server*))

;;; "webapp" goes here. Hacks and glory await!
