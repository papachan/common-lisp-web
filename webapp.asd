#|
   author: Dan Loaiza <papachan@gmail.com>
|#
(in-package :cl-user)
(defpackage :webapp-asd
  (:use :cl :asdf))
(in-package :webapp-asd)

(defsystem :webapp
  :version "0.1"
  :author "<papachan@gmail.com>"
  :license "MIT"
  :depends-on (:alexandria
               :clack
               :djula
               :jsonrpc
               :quri
               :yason)
  :description ""
  :serial t
  :components ((:module "src"
                :components
                ((:file "package")
                 (:file "webapp")))))
