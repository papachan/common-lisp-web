(in-package :cl-user)
(defpackage webapp-test
  (:use :cl :asdf))
(in-package :webapp-test)

(defsystem webapp-test
  :author "<papachan@gmail.com>"
  :license "MIT"
  :depends-on (:webapp
               :prove)
  :components ((:module "t"
                :components
                ((:test-file "webapp"))))
  :description ""

  :defsystem-depends-on (:prove-asdf)
  :perform (test-op :after (op c)
                    (funcall (intern #.(string :run) :prove) c)))
