
### A simple web app in common lisp with clack


(ql:quickload "webapp")
(in-package :webapp)


(webapp:start :port 8000 :debug nil)


(webapp:start :host 'http://mywebsite.com' :port 80)
